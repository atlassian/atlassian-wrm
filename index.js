#!/usr/bin/env node

exports.compile = require('./compile').compile;
exports.parsers = require('./moduleParsers');
